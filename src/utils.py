from enum import Enum, auto


class VaccinationStage(Enum):
    PARTIAL = auto()
    FULL = auto()


NAME_MAP = {
    "Negeri Sembilan": "ns",
    "Pulau Pinang": "pinang",
    "W.P. Kuala Lumpur": "kl",
    "W.P. Labuan": "labuan",
    "W.P. Putrajaya": "putrajaya",
}


def process_names(state: str) -> str:
    result = NAME_MAP.get(state)

    if result:
        return result
    else:
        return state.lower()
