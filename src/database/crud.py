from typing import Optional
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import func
import datetime
import sqlalchemy

from . import models
from utils import VaccinationStage


def new_cases(date: Optional[datetime.date], db: Session):
    latest_date = db.query(func.max(models.CaseState.date)).scalar_subquery()

    if date is not None:
        return (
            db.query(models.CaseState.state, models.CaseState.cases_new)
            .filter(models.CaseState.date == date)
            .all()
        )
    else:
        return (
            db.query(models.CaseState.state, models.CaseState.cases_new)
            .filter(models.CaseState.date == latest_date)
            .all()
        )


def recovered_cases(date: Optional[datetime.date], db: Session):
    latest_date = db.query(func.max(models.CaseState.date)).scalar_subquery()

    if date is None:
        return (
            db.query(models.CaseState.state, models.CaseState.cases_recovered)
            .filter(models.CaseState.date == latest_date)
            .all()
        )
    else:
        return (
            db.query(models.CaseState.state, models.CaseState.cases_recovered)
            .filter(models.CaseState.date == date)
            .all()
        )


def new_deaths(date: Optional[datetime.date], db: Session):
    latest_date = db.query(func.max(models.DeathState.date)).scalar_subquery()

    if date is None:
        return (
            db.query(models.DeathState.state, models.DeathState.deaths_new)
            .filter(models.DeathState.date == latest_date)
            .all()
        )
    else:
        return (
            db.query(models.DeathState.state, models.DeathState.deaths_new)
            .filter(models.DeathState.date == date)
            .all()
        )


def total_vaccinations(
    stage: VaccinationStage,
    start_date: Optional[datetime.date],
    end_date: Optional[datetime.date],
    db: Session,
):
    if start_date is None:
        start_date = db.query(
            func.min(models.NationalVaccinations.date)
        ).scalar_subquery()

    if end_date is None:
        end_date = db.query(
            func.max(models.NationalVaccinations.date)
        ).scalar_subquery()

    return (
        db.query(
            models.NationalVaccinations.date,
            models.NationalVaccinations.cumul_partial
            if stage == VaccinationStage.PARTIAL
            else models.NationalVaccinations.cumul_full,
        )
        .filter(
            models.NationalVaccinations.date >= start_date,
            models.NationalVaccinations.date <= end_date,
        )
        .all()
    )


def daily_full_dose(
    start_date: Optional[datetime.date], end_date: Optional[datetime.date], db: Session
):
    if start_date is None:
        start_date = db.query(
            func.min(models.NationalVaccinations.date)
        ).scalar_subquery()

    if end_date is None:
        end_date = db.query(
            func.max(models.NationalVaccinations.date)
        ).scalar_subquery()

    return (
        db.query(
            models.NationalVaccinations.date, models.NationalVaccinations.daily_full
        )
        .filter(
            models.NationalVaccinations.date >= start_date,
            models.NationalVaccinations.date <= end_date,
        )
        .all()
    )


def daily_partial_dose(
    start_date: Optional[datetime.date], end_date: Optional[datetime.date], db: Session
):
    if start_date is None:
        start_date = db.query(
            func.min(models.NationalVaccinations.date)
        ).scalar_subquery()

    if end_date is None:
        end_date = db.query(
            func.max(models.NationalVaccinations.date)
        ).scalar_subquery()

    return (
        db.query(
            models.NationalVaccinations.date, models.NationalVaccinations.daily_partial
        )
        .filter(
            models.NationalVaccinations.date >= start_date,
            models.NationalVaccinations.date <= end_date,
        )
        .all()
    )


def all_users(db: Session):
    return db.query(
        models.User.username,
        models.User.email,
        models.User.phone_number,
        models.User.last_login,
        models.User.login_count,
    ).all()


def user_add(user: models.User, db: Session):
    rows = db.query(models.User).count()
    if rows == 0:
        user.superuser = True

    try:
        db.add(user)
        db.commit()
        return True
    except sqlalchemy.exc.IntegrityError:
        db.rollback()
        return False


def get_user(username, db: Session):
    return db.query(models.User).filter(models.User.username == username).first()


def update_user_login(user: models.User, db: Session):
    user.login_count += 1
    user.last_login = datetime.datetime.utcnow()
    db.add(user)
    db.commit()
