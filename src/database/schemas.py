from datetime import date, datetime
from typing import List, Optional

from pydantic import BaseModel


class CaseState(BaseModel):
    date: date
    state: str
    cases_new: int
    cases_import: int
    cases_recovered: int

    class Config:
        orm_mode = True


class DataByState(BaseModel):
    johor: int
    kedah: int
    kelantan: int
    melaka: int
    ns: int
    pahang: int
    perak: int
    perlis: int
    pinang: int
    sabah: int
    sarawak: int
    selangor: int
    terengganu: int
    kl: int
    labuan: int
    putrajaya: int


class DataRange(BaseModel):
    dates: List[date]
    data: List[int]


class User(BaseModel):
    username: str
    email: str
    password: str
    phone_number: str


class UserInfo(BaseModel):
    username: str
    email: str
    phone_number: str
    last_login: Optional[datetime]
    login_count: int

    class Config:
        orm_mode = True
