from sqlalchemy import Boolean, Column, Date, DateTime, Integer, String, Text
from .database import Base


class CaseState(Base):
    __tablename__ = "cases_state"

    date = Column(Date, primary_key=True, index=True)
    state = Column(String, primary_key=True, index=True)
    cases_new = Column(Integer)
    cases_import = Column(Integer)
    cases_recovered = Column(Integer)


class DeathState(Base):
    __tablename__ = "deaths_state"

    date = Column(Date, primary_key=True, index=True)
    state = Column(String, primary_key=True, index=True)
    deaths_new = Column(Integer)
    deaths_new_dod = Column(Integer)
    deaths_bid = Column(Integer)
    deaths_bid_dod = Column(Integer)


class NationalVaccinations(Base):
    __tablename__ = "vax_malaysia"

    date = Column(Date, primary_key=True, index=True)
    daily_partial = Column(Integer)
    daily_full = Column(Integer)
    cumul_partial = Column(Integer)
    cumul_full = Column(Integer)


class User(Base):
    __tablename__ = "users"

    user_id = Column(Integer, primary_key=True, index=True)
    username = Column(String(30), index=True, unique=True, nullable=False)
    email = Column(String(254), index=True, unique=True, nullable=False)
    password = Column(Text, nullable=False)
    phone_number = Column(String(15), index=True, nullable=False)
    last_login = Column(DateTime(timezone=True), index=True)
    login_count = Column(Integer, index=True, default=0, nullable=False)
    superuser = Column(Boolean, index=True, default=False, nullable=False)
