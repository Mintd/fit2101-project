async function signup(event) {
    event.preventDefault();

    let input_username = document.getElementById("username_field").value;
    let input_password = document.getElementById("password_field").value;
    let input_password2 = document.getElementById("password_field2").value;
    let input_email = document.getElementById("email_field").value;
    let input_phonenumber = document.getElementById("phonenumber_field").value;

    if (input_password != input_password2) {
        document.getElementById("errormessage").innerText =
            "Passwords do not match";
    } else {
        try {
            await accountCreation(
                input_username,
                input_password,
                input_email,
                input_phonenumber
            );
            window.location.href = `http://${window.location.host}/login`;
        } catch {
            document.getElementById("errormessage").innerHTML =
                "Account details have already been used";
        }
    }
}

async function accountCreation(username, password, email, phone_number) {
    let signUpPath = "/signup";
    const response = await fetch(signUpPath, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({ username, password, email, phone_number }),
        redirect: "error",
    });
    if (response.status == 409) {
        throw new Error();
    }
}

const form = document.getElementById("signup-form");
form.addEventListener('submit', signup);
