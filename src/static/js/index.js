/* Helper Functions */
function displayState(id) {
  // Map of ID to State
  let states = {
    johor: "Johor",
    kedah: "Kedah",
    kelantan: "Kelantan",
    kl: "Kuala Lumpur",
    labuan: "Labuan",
    melaka: "Melaka",
    ns: "Negeri Sembilan",
    pahang: "Pahang",
    perak: "Perak",
    perlis: "Perlis",
    pinang: "Pulau Pinang",
    putrajaya: "Putrajaya",
    sabah: "Sabah",
    sarawak: "Sarawak",
    selangor: "Selangor",
    terengganu: "Terangganu"
  };

  // Return Appropriate State
  return states[id];
}

function dateHTML(date) {
  // Get Year, Month, and Date
  let year = date.getFullYear();
  let month = date.getMonth() + 1;
  let day = date.getDate();

  // Add Zero in front for appropriate months and days
  if (month < 10) {
    month = "0" + month;
  }
  if (day < 10) {
    day = "0" + day;
  }

  // Concatenate Values
  let dateHTML = year + "-" + month + "-" + day;
  return dateHTML;
}

function capFirstLetter(string) {
  return string[0].toUpperCase() + string.substring(1)
}


/* Table Widget Functions */
async function updateTable(category, path, date) {
  outputRef = document.getElementById("table");

  if (path === undefined) {
    path = "cases/new";
  }

  if (date === '' || date === undefined) {
    path = `/api/${path}`;
  } else {
    path = `/api/${path}?date=${date}`;
  }

  const response = await fetch(path);
  const object = await response.json();

  if (!response["ok"]) {
    outputRef.innerHTML = "<br><br><br><br><br><br><br><br><br><br><h5 style='text-align: center;'>No data for this date/category</h5>";
  } else {
    outputText = '<table style="width: 100%;"><tr><th style="width: 60%; text-align: left;">State</th><th style="text-align: left;">' + capFirstLetter(category) + '</th></tr>';
    for (let state in object) {
      outputText += '<tr><td>' + displayState(state) + '</td><td>' + object[state] + '</tr>';
    }
    outputText += '</table>';
    outputRef.innerHTML = outputText;
  }
}

async function changeTable() {
  const select = document.getElementById("categories");
  const datePicker = document.getElementById("tableDate");
  const category = select.options[select.selectedIndex].value;

  const mapping = {
    cases: "cases/new",
    deaths: "deaths/new",
    recoveries: "cases/recovery",
  }
  await updateTable(category, mapping[category], datePicker.value);
}


/* Single Digit Widget Function */
async function updateCases(date) {
  cases = `/api/cases/new`;

  if (date === '' || date === undefined) {} else {
    cases = `/api/cases/new?date=${date}`;
  }

  total_cases = 0;

  const response = await fetch(cases);
  const object = await response.json();

  if (response.status == 404) {
    throw new Error();
  }

  Object.keys(object).forEach((key) => {
    total_cases += object[key]
  });

  document.getElementById("cases").innerHTML = total_cases;
}

async function updateDeaths(date) {
  deaths = `/api/deaths/new`;

  if (date === '' || date === undefined) {} else {
    deaths = `/api/deaths/new?date=${date}`;
  }

  total_deaths = 0;

  const response = await fetch(deaths);

  if (response.status == 404) {
    if (new Date(date) < new Date("2020-01-25")) {
      throw new Error();
    }
  }

  const object = await response.json();

  Object.keys(object).forEach((key) => {
    if (typeof object[key] == "number") {
      total_deaths += object[key]
    }
  });
  document.getElementById("deaths").innerHTML = total_deaths;
}

async function updateRecoveries(date) {
  recoveries = `/api/cases/recovery`;

  if (date === '' || date === undefined) {} else {
    recoveries = `/api/cases/recovery?date=${date}`;
  }

  total_recoveries = 0;

  const response = await fetch(recoveries);

  if (response.status == 404) {
    throw new Error();
  }

  const object = await response.json();
  Object.keys(object).forEach((key) => {
    total_recoveries += object[key]
  });

  document.getElementById("recoveries").innerHTML = total_recoveries;
}

async function changeValues() {
  const datePicker = document.getElementById("numberDate");

  try {
    await updateCases(datePicker.value);
    await updateDeaths(datePicker.value);
    await updateRecoveries(datePicker.value);
    document.getElementById("nationalDataError").style.display = "none";
    document.getElementById("nationalData").style.display = "";
  } catch {
    document.getElementById("nationalDataError").style.display = "";
    document.getElementById("nationalData").style.display = "none";
  }
}


/* Global Code */
// Get today's date and last week's date
today = new Date();
lastWeek = new Date(new Date().setDate(new Date().getDate() - 7));

// Get Date Inputs
tableDateRef = document.getElementById("tableDate")
numberDateRef = document.getElementById("numberDate")
graphStartRef = document.getElementById("graphStart")
graphEndRef = document.getElementById("graphEnd")

// Default Date Inputs to appropriate values
tableDateRef.value = dateHTML(today);
numberDateRef.value = dateHTML(today);
graphStartRef.value = dateHTML(lastWeek);
graphEndRef.value = dateHTML(today);

// Update Table Widget
updateTable("cases", "cases/new", tableDateRef.value);

// Update Single Digit Widget
changeValues();
