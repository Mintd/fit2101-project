const API_PATH = "/api/users/all";
const KEYS = ["username", "email", "phone_number", "last_login", "login_count"];
const DATE_FORMAT = {
	hour: "numeric",
	minute: "numeric",
	second: "numeric",
}

async function show_all_users() {
    const response = await fetch(API_PATH);
    const user_list = await response.json();
	console.log(user_list);

    const table = document.getElementById("userData");

    user_list.forEach((user) => {
        const row = document.createElement("tr");
        KEYS.forEach((key) => {
            const cell = document.createElement("td");

            if (key == "last_login" && user[key]) {
                const date = new Date(`${user[key]}Z`);
                cell.innerHTML = date.toLocaleDateString("en-MY", DATE_FORMAT);
            } else if (key == "last_login") {
                cell.innerHTML = "User has not logged in";
            } else {
                cell.innerHTML = user[key];
            }

			row.appendChild(cell);
        });
        table.appendChild(row);
    });
}

show_all_users();
