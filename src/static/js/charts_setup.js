const baseUrl = window.location.origin;
const dailyPartialColor = {
  backgroundColor: "#0dbf5a",
  borderColor: "#0dbf5a",
};
const dailyFullColor = {
  backgroundColor: "#1cacff",
  borderColor: "#1cacff",
};
const totalPartialColor = {
  backgroundColor: "#078a37",
  borderColor: "#078a37",
};
const totalFullColor = {
  backgroundColor: "#2164ff",
  borderColor: "#2164ff",
};

let dailyPartial;
let dailyFull;
let totalPartial;
let totalFull;

function updateCharts() {
  const start_date = document.getElementById("graphStart");
  const end_date = document.getElementById("graphEnd");
  const requestOptions = new URLSearchParams();

  if (new Date(start_date.value) > new Date(end_date.value)) {
    document.getElementById("chartArea").style.display = "none";
    document.getElementById("chartAreaError").style.display = "";
    return null;
  } else {
    document.getElementById("chartArea").style.display = "";
    document.getElementById("chartAreaError").style.display = "none";
  }

  if (start_date.value) {
    requestOptions.append("start_date", start_date.value);
  }
  if (end_date.value) {
    requestOptions.append("end_date", end_date.value);
  }

  updateDailyPartialChart(requestOptions);
  updateDailyFullChart(requestOptions);
  updateTotalPartialChart(requestOptions);
  updateTotalFullChart(requestOptions);
}

async function updateDailyPartialChart(options) {
  const request = new URL("api/vaccinations/daily/partial", baseUrl);
  request.search = options;
  const response = await fetch(request);
  const data = await response.json();
  
  if (data.dates.length == 0) {
    document.getElementById("chartArea").style.display = "none";
    document.getElementById("chartAreaError").style.display = "";
  }

  if (dailyPartial) {
    dailyPartial.destroy();
  }

  dailyPartial = new Chart("dailyPartial", {
    type: "line",
    data: {
      labels: data.dates,
      datasets: [
        { label: "Daily Partial Vaccinations", data: data.data, ...dailyPartialColor },
      ],
    },
  });
}

async function updateDailyFullChart(options) {
  const request = new URL("api/vaccinations/daily/full", baseUrl);
  request.search = options;
  const response = await fetch(request);
  const data = await response.json();

  if (dailyFull) {
    dailyFull.destroy();
  }

  dailyFull = new Chart("dailyFull", {
    type: "line",
    data: {
      labels: data.dates,
      datasets: [
        { label: "Daily Full Vaccinations", data: data.data, ...dailyFullColor },
      ],
    },
  });
}

async function updateTotalPartialChart(options) {
  const request = new URL("api/vaccinations/total/partial", baseUrl);
  request.search = options;
  const response = await fetch(request);
  const data = await response.json();

  if (totalPartial) {
    totalPartial.destroy();
  }

  totalPartial = new Chart("totalPartial", {
    type: "line",
    data: {
      labels: data.dates,
      datasets: [
        { label: "Total Partial Vaccinations", data: data.data, ...totalPartialColor },
      ],
    },
  });
}

async function updateTotalFullChart(options) {
  const request = new URL("api/vaccinations/total/full", baseUrl);
  request.search = options;
  const response = await fetch(request);
  const data = await response.json();

  if (totalFull) {
    totalFull.destroy();
  }

  totalFull = new Chart("totalFull", {
    type: "line",
    data: {
      labels: data.dates,
      datasets: [
        { label: "Total Full Vaccinations", data: data.data, ...totalFullColor },
      ],
    },
  });
}

updateCharts();
