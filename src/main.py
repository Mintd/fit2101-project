from typing import List, Optional
import datetime

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.requests import Request
from fastapi.responses import FileResponse, RedirectResponse
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session

from auth import UnauthorizedException, auth_handler, manager
import database.crud
from database import crud, models, database, schemas
import utils

models.Base.metadata.create_all(bind=database.engine)
api = FastAPI()
app = FastAPI()
app.mount("/api", api)
app.mount("/static", StaticFiles(directory="static", html=True))
templates = Jinja2Templates(directory="templates")

app.add_exception_handler(UnauthorizedException, auth_handler)
api.add_exception_handler(UnauthorizedException, auth_handler)


# Dependency
def get_db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()


@manager.user_loader(db=database.SessionLocal())
def load_user(username: str, db: Session):
    user = crud.get_user(username, db)
    return user


@api.get("/cases/new", response_model=schemas.DataByState)
async def get_new_cases(
    date: Optional[datetime.date] = None,
    db: Session = Depends(get_db),
    _=Depends(manager),
):
    data = crud.new_cases(date, db)
    if len(data) == 0:
        raise HTTPException(status_code=404, detail="Item not found")
    else:
        return {
            utils.process_names(case.state): case.cases_new
            for case in crud.new_cases(date, db)
        }


@api.get("/cases/recovery", response_model=schemas.DataByState)
async def get_recovered_cases(
    date: Optional[datetime.date] = None,
    db: Session = Depends(get_db),
    _=Depends(manager),
):
    rec_cases = crud.recovered_cases(date, db)
    if len(rec_cases) == 0:
        raise HTTPException(status_code=404, detail="No data present on that date")
    else:
        return {
            utils.process_names(case.state): case.cases_recovered
            for case in crud.recovered_cases(date, db)
        }


@api.get("/deaths/new", response_model=schemas.DataByState)
async def get_new_deaths(
    date: Optional[datetime.date] = None,
    db: Session = Depends(get_db),
    _=Depends(manager),
):
    data = crud.new_deaths(date, db)
    if len(data) == 0:
        raise HTTPException(status_code=404, detail="No data present on that date")
    else:
        return {
            utils.process_names(death.state): death.deaths_new
            for death in crud.new_deaths(date, db)
        }


@api.get("/vaccinations/total/partial", response_model=schemas.DataRange)
async def get_total_partial_vaccinations(
    start_date: Optional[datetime.date] = None,
    end_date: Optional[datetime.date] = None,
    db: Session = Depends(get_db),
    _=Depends(manager),
):
    result = crud.total_vaccinations(
        utils.VaccinationStage.PARTIAL, start_date, end_date, db
    )

    return {
        "dates": [item.date for item in result],
        "data": [item.cumul_partial for item in result],
    }


@api.get("/vaccinations/total/full", response_model=schemas.DataRange)
async def get_total_full_vaccinations(
    start_date: Optional[datetime.date] = None,
    end_date: Optional[datetime.date] = None,
    db: Session = Depends(get_db),
    _=Depends(manager),
):
    result = crud.total_vaccinations(
        utils.VaccinationStage.FULL, start_date, end_date, db
    )

    return {
        "dates": [item.date for item in result],
        "data": [item.cumul_full for item in result],
    }


@api.get("/vaccinations/daily/full", response_model=schemas.DataRange)
async def get_daily_full_dose(
    start_date: Optional[datetime.date] = None,
    end_date: Optional[datetime.date] = None,
    db: Session = Depends(get_db),
    _=Depends(manager),
):

    data = crud.daily_full_dose(start_date, end_date, db)

    return {
        "dates": [item.date for item in data],
        "data": [item.daily_full for item in data],
    }


@api.get("/vaccinations/daily/partial", response_model=schemas.DataRange)
async def get_daily_partial_dose(
    start_date: Optional[datetime.date] = None,
    end_date: Optional[datetime.date] = None,
    db: Session = Depends(get_db),
    _=Depends(manager),
):

    data = crud.daily_partial_dose(start_date, end_date, db)

    return {
        "dates": [item.date for item in data],
        "data": [item.daily_partial for item in data],
    }


@api.get("/users/all", response_model=List[schemas.UserInfo])
async def get_all_users(db: Session = Depends(get_db)):
    return crud.all_users(db)


@app.get("/")
async def main(request: Request, user: models.User = Depends(manager)):
    return templates.TemplateResponse(
        "index.html", {"request": request, "superuser": user.superuser}
    )


@app.get("/signup")
async def signup_page(request: Request):
    try:
        await manager(request)
        return RedirectResponse("/")
    except UnauthorizedException:
        return FileResponse("html/signup.html")


@app.post("/signup")
async def signup(data: schemas.User, db: Session = Depends(get_db)):
    user = models.User(
        username=data.username,
        email=data.email,
        password=data.password,
        phone_number=data.phone_number,
    )
    added = crud.user_add(user, db)

    if not added:
        raise HTTPException(status.HTTP_409_CONFLICT)


@app.get("/login")
async def login_page(request: Request):
    try:
        await manager(request)
        return RedirectResponse("/")
    except UnauthorizedException:
        return templates.TemplateResponse(
            "login.html", {"request": request, "invalid": False}
        )


@app.post("/login")
def login(
    request: Request,
    data: OAuth2PasswordRequestForm = Depends(),
    db: Session = Depends(get_db),
):
    username = data.username
    password = data.password
    user = load_user(username, db)

    if not user or password != user.password:
        return templates.TemplateResponse(
            "login.html", {"request": request, "invalid": True}
        )

    crud.update_user_login(user, db)

    response = RedirectResponse("/", status_code=status.HTTP_303_SEE_OTHER)
    access_token = manager.create_access_token(
        data=dict(sub=username), expires=datetime.timedelta(hours=1)
    )
    manager.set_cookie(response, access_token)

    return response


@app.get("/superuser")
def superuser(user: models.User = Depends(manager)):
    if not user.superuser:
        return RedirectResponse("/")

    return FileResponse("html/superuser.html")
