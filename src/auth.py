from fastapi.responses import RedirectResponse
from fastapi_login.fastapi_login import LoginManager


SECRET = "your-secret-key"


class UnauthorizedException(Exception):
    pass


def auth_handler(_, __):
    return RedirectResponse(url="/login")


manager = LoginManager(
    SECRET,
    token_url="/auth/token",
    use_cookie=True,
)
manager.not_authenticated_exception = UnauthorizedException
