# FIT2101 Group C5

This Git Repository contains the appropriate documentation and code for Group C5 of FIT2101. 

- Google Drive: https://drive.google.com/drive/folders/0AGfkwi2aHbkmUk9PVA
- Trello: https://trello.com/b/uhpIxOIf/fit2101

## Setup

First, create a Python virtual environment for this project:

```sh
python -m venv --upgrade-deps venv
```

You can switch to this virtual environment by running this command:

```sh
venv/Scripts/activate
```

If you're using VSCode, it will automatically start your terminal with this
virtual environment activated.

Then, install all the required dependencies of this project by running this:

```sh
pip install -r requirements.txt
```

In the `src` directory, run the following command to start the Python server:

```sh
uvicorn main:app --reload
```

This server auto reloads on code change, which is convenient for development.
This will then run the server, which will create the necessary database files
if they don't already exist.

To import the CSV files, download [DB Browser for SQLite](https://sqlitebrowser.org/),
and download all the required CSV files from
[here](https://github.com/MoH-Malaysia/covid19-public).

Open the DB file with the program, and go to `File -> Import -> Table from CSV file...`
and import the CSV files you downloaded, and use the settings shown in the
picture below (excluding table name):

![Import Screen](docs/ImportScreen.png)

## Team Members

- **Arthur Lee** (31435939): Scrum Master
- **Koh Jia Hao** (31438962): Product Owner
- **Wong Yi Xiong** (29801109): Team Member
- **Ong Chang Zheng** (3155209): Team Member
- **Ethan Hor** (29938767): Team Member
- **Mohamed Nabhaan Ali** (31258646): Team Member

## Project Inception

The following deliverables were added for the Project Inception.

- ProjectPlan
- AnalysisOfAlternatives
- RiskRegister

## Project Iteration 1

The folder structure after Project Iteration 1 is as follows.

* docs
    - AnalysisOfAlternatives.pdf
    - ImportScreen.png (NEW)
    - ProjectPlan.pdf (UPDATED)
    - RetrospectiveSprint1.pdf (NEW)
    - RiskRegister.pdf
* src
    - database (NEW)
        + crud.py (NEW)
        + database.py (NEW)
        + models.py (NEW)
        + schemas.py (NEW)
    - static (NEW)
        + css (NEW)
            + icons.css (NEW)
        + img (NEW)
            + Burger-King-Logo-1999.jpg (NEW)
            + depositphotos_119659092-stock-illustration-male-avatar-profile-picture-vector.jpg (NEW)
            + download.png (NEW)
        + js (NEW)
            + index.js (NEW)
        + index.html (NEW)
    - main.py (NEW)
    - utils.py (NEW)
    - requirements.txt (NEW)

Note that time tracking for Sprint 1 is done in the Google Sheets file TimeTracker in our Google Drive. The Time Management Section in the ProjectPlan has been updated to reflect the new method we have chosen for time tracking.

## Project Iteration 2

The folder structure after Project Iteration 2 is as follows.

* docs
    - AnalysisOfAlternatives.pdf
    - ImportScreen.png 
    - ProjectPlan.pdf 
    - RetrospectiveSprint1.pdf 
    - RetrospectiveSprint2.pdf (NEW)
    - RiskRegister.pdf
* src
    - database 
        + crud.py 
        + database.py 
        + models.py 
        + schemas.py 
    - static 
        + css 
            + icons.css 
            + *signup.css* (For Sprint 3)
            + styles.css (NEW)
        + img 
            + Burger-King-Logo-1999.jpg 
            + depositphotos_119659092-stock-illustration-male-avatar-profile-picture-vector.jpg 
            + download.png 
        + js 
            + chart_setup.js (NEW)
            + index.js 
        + index.html 
        + *signup.html* (For Sprint 3)
    - main.py
    - utils.py 
    - requirements.txt

Time management is done in Git for this Sprint.

## Project Iteration 3

The folder structure after Project Iteration 3 is as follows.

* docs
    - AnalysisOfAlternatives.pdf
    - ImportScreen.png 
    - ProjectPlan.pdf 
    - RetrospectiveSprint1.pdf 
    - RetrospectiveSprint2.pdf 
    - RetrospectiveSprint3.pdf (NEW)
    - RiskRegister.pdf
* src
    - database 
        + crud.py 
        + database.py 
        + models.py 
        + schemas.py 
    - html (NEW)
        + signup.html (NEW)
        + superuser.html (NEW)
    - static 
        + css 
            + icons.css 
            + signup.css (NEW)
            + styles.css 
        + img 
            + Burger-King-Logo-1999.jpg 
            + depositphotos_119659092-stock-illustration-male-avatar-profile-picture-vector.jpg 
            + download.png 
            + signup.png (NEW)
        + js 
            + chart_setup.js 
            + index.js 
            + signup.js (NEW)
            + superuser.js (NEW)
        + mp4 (NEW)
            + demo_video.mp4 (NEW)
    - templates (NEW)
        + index.html 
        + login.html (NEW)
    - auth.py (NEW)
    - main.py
    - utils.py 
    - requirements.txt

Time management is done in Google Sheets for this Sprint.
